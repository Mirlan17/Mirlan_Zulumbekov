from django.views.generic import TemplateView

class HomeView(TemplateView):
    template_name = "Main.html"

class NewsView(TemplateView):
    template_name = "news.html"

class LifeView(TemplateView):
    template_name = "life.html"

class AboutView(TemplateView):
    template_name = "about.html"

class KonkursView(TemplateView):
    template_name = "kurs.html"
